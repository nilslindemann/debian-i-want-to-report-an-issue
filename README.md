# DEBIAN I WANT TO REPORT AN ISSUE

I am talking to [devscripts](https://salsa.debian.org/debian/devscripts).

When I do ...

```
python -m pip install --upgrade pip
```

... in Linux Mint, I get this message (linebreaks inserted by me) ...

```
(...)
DEPRECATION: devscripts 2.22.1ubuntu1 has a non-standard version number. pip 24.0 will enforce this behaviour change.
A possible replacement is to upgrade to a newer version of devscripts or contact the author to suggest
that they release a version with a conforming version number.
Discussion can be found at https://github.com/pypa/pip/issues/12063
```

... which informs me to inform you (`devscripts`). Which I am doing right now. As I do not see how to create an issue in [your repository](https://salsa.debian.org/debian/devscripts), I do it this way.

Latest Linux Mint,

```
> python --version
Python 3.10.12
> pip --version
pip 23.3 from /home/nils/.local/lib/python3.10/site-packages/pip (python 3.10)
``` 

2023-10-19
